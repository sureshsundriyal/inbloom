// Copyright (c) 2013, Suresh Sundriyal. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file.

package bitvector

import "bitbucket.org/sureshsundriyal/inbloom/arch"

type BitVector struct {
	vector  []uint
	NumBits uint64
}

// NewBitVector allocates a bitvector and returns a reference to it.
func NewBitVector(numBits uint64) *BitVector {
	numWords := (numBits / arch.WORDSIZE)
	if (numBits % arch.WORDSIZE) != 0 {
		numWords += 1
	}
	vector := make([]uint, numWords)
	return &BitVector{vector, numWords * arch.WORDSIZE}
}

// SetBit sets the specified bit in the bitvector.
func (bv *BitVector) SetBit(bit uint64) {
	bv.vector[bit/arch.WORDSIZE] |= 1 << (bit % arch.WORDSIZE)
}

// UnsetBit unsets the specified bit in the bitvector.
func (bv *BitVector) UnsetBit(bit uint64) {
	bv.vector[bit/arch.WORDSIZE] &= ^(1 << (bit % arch.WORDSIZE))
}

// Bit tests the specified bit.
func (bv *BitVector) Bit(bit uint64) bool {
	return ((bv.vector[bit/arch.WORDSIZE] & (1 << (bit % arch.WORDSIZE))) != 0)
}
