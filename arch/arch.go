// Copyright (c) 2013, Suresh Sundriyal. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file.

// Package arch provides information about the system architecture.
package arch

import "unsafe"

// BIGENDIAN set to true indicates a big endian system.
var BIGENDIAN bool
// LITTLEENDIAN set to true indicates a little endian system.
var LITTLEENDIAN bool

const (
	_m       = ^uint(0)                      // All bits set to 1
	_logS    = _m>>8&1 + _m>>16&1 + _m>>32&1 // log
	sizePtr  = 1 << _logS                    // Number of bytes in a uint
	//WORDSIZE is set to the system word size.
	WORDSIZE = sizePtr << 3
)

func init() {
	//test and set the endianness of the system.
	var i uint32 = 0xFFAABBCC
	x := (*byte)(unsafe.Pointer(&i))
	if *x == 0xCC {
		LITTLEENDIAN = true
		return
	}
	BIGENDIAN = true
}

