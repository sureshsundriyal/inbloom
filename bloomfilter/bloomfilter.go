// Copyright (c) 2013, Suresh Sundriyal. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file.

package bloomfilter

import (
	"bitbucket.org/sureshsundriyal/inbloom/bitvector"
	"hash"
	"hash/fnv"
)

type BloomFilter struct {
	bv        *bitvector.BitVector
	numHashes int
	hash1     hash.Hash64
	hash2     hash.Hash64
}

var b = make([]byte, 8)

// NewBloomFilter allocates a BloomFilter structure and returns a reference.
// numBits : The number of bits contained in the bloomfilter.
// numHashes : The number of hashes used by the bloomfilter.
func NewBloomFilter(numBits uint64, numHashes int) *BloomFilter {
	return &BloomFilter{bitvector.NewBitVector(numBits), numHashes,
		fnv.New64(), fnv.New64a()}
}

// calculateHash calculates the required hashes.
func calculateHash(h1 hash.Hash64, h2 hash.Hash64, key []byte) (uint64, uint64) {
	h1.Reset()
	h2.Reset()

	h1.Write(key)
	h := h1.Sum64()
	//	h2.Write([]byte{byte(h >> 56), byte(h >> 48), byte(h >> 40), byte(h >> 32),
	//		byte(h >> 24), byte(h >> 16), byte(h >> 8), byte(h)})

	//convert h to a byte array.
	z := uint8(7)
	for i := uint8(0); i < 8; i++ {
		b[i] = byte((h) >> (z * 8))
		z--
	}

	h2.Write(b)
	h2.Write(key)

	return h, h2.Sum64()
}

// AddKey method adds the key to the bloomfilter.
func (bf *BloomFilter) AddKey(key []byte) {
	h1, h2 := calculateHash(bf.hash1, bf.hash2, key)

	keyLen := len(key) - 1
	keyCounter := keyLen

	for i := bf.numHashes; i > 0; i-- {
		if keyCounter < 0 {
			keyCounter = keyLen
		}

		bf.bv.SetBit((h1 + (uint64(key[keyCounter]) * uint64(i) * h2)) %
			uint64(bf.bv.NumBits))

		keyCounter -= 1

	}
}

// IsPresent checks to see if the key is present in the bloomfilter or not.
// If the returned value is false the key is definitely not in the bloomfilter,
// on the other hand if the returned value is false, the key may or may not be
// present.
func (bf *BloomFilter) IsPresent(key []byte) bool {
	h1, h2 := calculateHash(bf.hash1, bf.hash2, key)

	keyLen := len(key) - 1
	keyCounter := keyLen

	for i := bf.numHashes; i > 0; i-- {
		if keyCounter < 0 {
			keyCounter = keyLen
		}

		if !bf.bv.Bit((h1 + (uint64(key[keyCounter]) * uint64(i) * h2)) %
			uint64(bf.bv.NumBits)) {
			return false
		}

		keyCounter -= 1

	}
	return true
}
