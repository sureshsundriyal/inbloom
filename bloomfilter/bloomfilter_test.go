// Copyright (c) 2013, Suresh Sundriyal. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file.

package bloomfilter

import (
	"math"
	"testing"
)

const (
	counter   uint64 = 6400000
	bitFactor uint64 = 30
)

// TestNew just allocates a bloomfilter with a large number of bits.
func TestNew(t *testing.T) {
	NewBloomFilter(64000000000, 10)
}

func putUint(b []byte, v *uint64) []byte {
	for i := uint8(0); i < 8; i++ {
		b[i] = byte(*v >> (i * 8))
	}
	return b
}

// TestBasicFuntionality tests for the basic functionality.
func TestBasicFunctionality(t *testing.T) {
	bf := NewBloomFilter(counter*bitFactor, int(float64(bitFactor)*math.Log(2)))
	b := make([]byte, 8)

	for i := uint64(0); i < counter; i++ {
		bf.AddKey(putUint(b, &i))
	}

	failureCount := 0
	var x uint64 = 0
	for i := uint64(0); i < counter; i++ {
		// All of these should definitely be present since we added them.
		if !bf.IsPresent(putUint(b, &i)) {
			t.Error("False Negative:", i)
			t.FailNow()
		}

		x = i + counter
		if bf.IsPresent(putUint(b, &x)) {
			failureCount += 1
		}

	}

	// This is bound to fail but should give us some statistic
	// as to the correctness of the implementation.
	if true {
		t.Error("BitFactor:", bitFactor)
		t.Error("Number of hashes:", uint64(float64(bitFactor)*math.Log(2)))
		t.Error("Number of false positives:", failureCount)
		t.Error("Number of bits:", bf.bv.NumBits)
		t.Error("False positive rate:", float64(failureCount)/float64(counter)/100)
	}
}
